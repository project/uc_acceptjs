(function ($) {

  $(document).ready(function () {
    // Scroll to top so the modal is visible.
    $('.acceptjs-submit-btn').click(function () {
      if ("ontouchstart" in window || navigator.msMaxTouchPoints) {
        console.log("AuthorizeNet bug workaround")
        $("#AcceptUIContainer").addClass('show');
        $("#AcceptUIBackground").addClass('show');
      }
      window.scrollTo(0, 0);
    });
  });


})(jQuery);

// Accept.js callbacks

/**
 * Callback to display errors or submit the form.
 */
function uc_acceptjs_responder(response) {
  (function ($) {
    if (response.messages.resultCode === "Error") {
      var i = 0;
      while (i < response.messages.message.length) {
        $('#review-instructions').html('<div class="messages error">' + response.messages.message[i].text + ' ' + 'Please try again.</div>');
        console.log(
          response.messages.message[i].code + ": " +
          response.messages.message[i].text
          );
        i = i + 1;
      }
    } else {
      paymentFormUpdate(response.opaqueData);
    }
  })(jQuery);
}

/**
 * Callback to update the payment form with data and then submit it.
 */
function paymentFormUpdate(opaqueData) {
  // Show overlay so user does not attempt to re-submit form while processing.
  document.getElementById("acceptjs-processing-overlay").style.display = 'block';

  document.getElementById("dataDescriptor").value = opaqueData.dataDescriptor;
  document.getElementById("dataValue").value = opaqueData.dataValue;

  // If using your own form to collect the sensitive data from the customer,
  // blank out the fields before submitting them to your server.
  // document.getElementById("cardNumber").value = "";
  // document.getElementById("expMonth").value = "";
  // document.getElementById("expYear").value = "";
  // document.getElementById("cardCode").value = "";

  document.getElementById("paymentForm").submit();
}